package com.nguyenvanquan7826.testlistuserwithsex;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<User> userList = new ArrayList<>();
    private ListView lvUser;
    private ArrayAdapter<User> adapter;

    private EditText editName, editAge;
    private RadioGroup radGroupSex;

    private int editPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editAge = findViewById(R.id.editAge);
        editName = findViewById(R.id.editName);
        radGroupSex = findViewById(R.id.radSex);

        lvUser = findViewById(R.id.lvUser);
        adapter = new ArrayAdapter<User>(this, 0, userList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.user_item, null);

                TextView tvName = convertView.findViewById(R.id.tvName);
                TextView tvAge = convertView.findViewById(R.id.tvAge);
                ImageView ivUser = convertView.findViewById(R.id.ivUser);

                User u = userList.get(position);

                tvName.setText(u.getName());
                tvAge.setText(u.getAge() + "");

                ivUser.setImageResource(R.drawable.ic_male);
                if (!u.isMale()) {
                    ivUser.setImageResource(R.drawable.ic_female);
                }

                return convertView;
            }
        };
        lvUser.setAdapter(adapter);

        findViewById(R.id.btnAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUser();
            }
        });
        findViewById(R.id.btnUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
            }
        });
        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                clickItem(position);
            }
        });
    }

    private void addUser() {
        String name = editName.getText().toString().trim();
        String ageText = editAge.getText().toString().trim();
        int age = Integer.parseInt(ageText);

        int idCheck = radGroupSex.getCheckedRadioButtonId();

        User user = new User();
        user.setName(name);
        user.setAge(age);

        if (idCheck == R.id.radMale) user.setMale(true);
        else user.setMale(false);

        userList.add(user);
        adapter.notifyDataSetChanged();
    }

    private void update() {
        if (editPosition >= 0) {
            String name = editName.getText().toString().trim();
            String ageText = editAge.getText().toString().trim();
            int age = Integer.parseInt(ageText);

            int idCheck = radGroupSex.getCheckedRadioButtonId();

            User user = new User();
            user.setName(name);
            user.setAge(age);

            if (idCheck == R.id.radMale) user.setMale(true);
            else user.setMale(false);

            userList.set(editPosition, user);
            adapter.notifyDataSetChanged();
        }

        editPosition = -1;
    }

    private void clickItem(int position) {
        User u = userList.get(position);
        editName.setText(u.getName());
        editAge.setText(u.getAge() + "");
        if (u.isMale()) {
            ((RadioButton) findViewById(R.id.radMale)).setChecked(true);
        } else {
            ((RadioButton) findViewById(R.id.radFeMale)).setChecked(true);
        }

        editPosition = position;
    }
}
