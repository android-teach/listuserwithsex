package com.nguyenvanquan7826.testlistuserwithsex;

public class User {
    private String name;
    private int age;
    boolean isMale;

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public int getAge() {
        return age;
    }

    public User setAge(int age) {
        this.age = age;
        return this;
    }

    public boolean isMale() {
        return isMale;
    }

    public User setMale(boolean male) {
        isMale = male;
        return this;
    }
}
